//
//  ViewController.m
//  SIGN UP
//
//  Created by Click Labs134 on 9/17/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UIImageView *backgroundImage;
UILabel *signUp;
UILabel *firstName;
UILabel *lastName;
UILabel *email;
UILabel *phoneNumber;
UILabel *password;
UILabel *reEnterPassword;
UITextField *firstNameTextField;
UITextField *lastNameTextField;
UITextField *emailTextField;
UITextField *phoneNumberTextField;
UITextField *passwordTextField;
UITextField *reEnterPasswordTextField;
UIButton *submitButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    //for background image
    backgroundImage=[[UIImageView alloc]init];
   backgroundImage .frame = CGRectMake(0, 0, 320, 570);
    backgroundImage .image = [UIImage imageNamed:@"1.jpg"];
 //   self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"b.jpg"]];
    [self.view addSubview:backgroundImage];
    
    //Creating Sign up label
    signUp=[[UILabel alloc]init];
    signUp.frame=CGRectMake(80, 50, 170, 35);
    signUp.textColor=[UIColor whiteColor];
    signUp.text=@"SIGN UP";
    [signUp setFont:[UIFont boldSystemFontOfSize:40]];
    [self.view addSubview:signUp];
    
    //Creating first name label
    firstName=[[UILabel alloc]init];
    firstName.frame=CGRectMake(10, 140, 150, 35);
    firstName.textColor=[UIColor whiteColor];
    firstName.text=@"First Name";
    [firstName setFont:[UIFont boldSystemFontOfSize:23]];
    [self.view addSubview:firstName];
    
    //Creating last name label
    lastName=[[UILabel alloc]init];
    lastName.frame=CGRectMake(10, 190, 150, 35);
    lastName.textColor=[UIColor whiteColor];
    lastName.text=@"Last Name";
    [lastName setFont:[UIFont boldSystemFontOfSize:23]];
    [self.view addSubview:lastName];
    
    //Creating email label
    email=[[UILabel alloc]init];
    email.frame=CGRectMake(10, 238, 150, 35);
    email.textColor=[UIColor whiteColor];
    email.text=@"Email";
    [email setFont:[UIFont boldSystemFontOfSize:23]];
    [self.view addSubview:email];
    
    //Creating phone number label
    phoneNumber=[[UILabel alloc]init];
    phoneNumber.frame=CGRectMake(10, 285, 150, 35);
    phoneNumber.textColor=[UIColor whiteColor];
    phoneNumber.text=@"Phone No.";
    [phoneNumber setFont:[UIFont boldSystemFontOfSize:23]];
    [self.view addSubview:phoneNumber];
    
    //Creating password label
    password=[[UILabel alloc]init];
    password.frame=CGRectMake(10, 333, 150, 35);
    password.textColor=[UIColor whiteColor];
    password.text=@"Password";
    [password setFont:[UIFont boldSystemFontOfSize:23]];
    [self.view addSubview:password];
    
    //Creating reEnterPassword label
    reEnterPassword=[[UILabel alloc]init];
    reEnterPassword.frame=CGRectMake(10, 380, 200, 35);
    reEnterPassword.textColor=[UIColor whiteColor];
    reEnterPassword.text=@"Re-Enter";
    [reEnterPassword setFont:[UIFont boldSystemFontOfSize:23]];
    [self.view addSubview:reEnterPassword];
    
    //Creating First Name TextFIeld
    firstNameTextField=[[UITextField alloc]init];
    firstNameTextField.placeholder=@"Enter first name";
    firstNameTextField.textColor=[UIColor blackColor];
    firstNameTextField.borderStyle=UITextBorderStyleRoundedRect;
    firstNameTextField.backgroundColor=[UIColor whiteColor];
    firstNameTextField.frame=CGRectMake(165, 140, 150, 35);
    [self.view addSubview:firstNameTextField];
    
    //Creating Last Name TextFIeld
    lastNameTextField=[[UITextField alloc]init];
    lastNameTextField.placeholder=@"Enter last name";
    lastNameTextField.textColor=[UIColor blackColor];
    lastNameTextField.borderStyle=UITextBorderStyleRoundedRect;
    lastNameTextField.backgroundColor=[UIColor whiteColor];
    lastNameTextField.frame=CGRectMake(165, 190, 150, 35);
    [self.view addSubview:lastNameTextField];
    
    //Creating email TextFIeld
    emailTextField=[[UITextField alloc]init];
    emailTextField.placeholder=@"Enter email";
    emailTextField.textColor=[UIColor blackColor];
    emailTextField.borderStyle=UITextBorderStyleRoundedRect;
    emailTextField.backgroundColor=[UIColor whiteColor];
    emailTextField.frame=CGRectMake(165, 238, 150, 35);
    [self.view addSubview:emailTextField];
    
    //Creating phone number TextFIeld
    phoneNumberTextField=[[UITextField alloc]init];
    phoneNumberTextField.placeholder=@"Enter phone number";
    phoneNumberTextField.textColor=[UIColor blackColor];
    phoneNumberTextField.borderStyle=UITextBorderStyleRoundedRect;
    phoneNumberTextField.backgroundColor=[UIColor whiteColor];
    phoneNumberTextField.frame=CGRectMake(165, 285, 150, 35);
    [self.view addSubview:phoneNumberTextField];
    
    //Creating password TextFIeld
    passwordTextField=[[UITextField alloc]init];
    passwordTextField.placeholder=@"Enter password";
    passwordTextField.textColor=[UIColor blackColor];
    passwordTextField.borderStyle=UITextBorderStyleRoundedRect;
    passwordTextField.backgroundColor=[UIColor whiteColor];
    passwordTextField.frame=CGRectMake(165, 333, 150, 35);
    passwordTextField.secureTextEntry=YES;
    [self.view addSubview:passwordTextField];
    
    //Creating reEnterPassword TextFIeld
    reEnterPasswordTextField=[[UITextField alloc]init];
    reEnterPasswordTextField.placeholder=@"Enter password";
    reEnterPasswordTextField.textColor=[UIColor blackColor];
    reEnterPasswordTextField.borderStyle=UITextBorderStyleRoundedRect;
    reEnterPasswordTextField.backgroundColor=[UIColor whiteColor];
    reEnterPasswordTextField.frame=CGRectMake(165, 380, 150, 35);
    reEnterPasswordTextField.secureTextEntry=YES;
    [self.view addSubview:reEnterPasswordTextField];
    
    //Creating Submit Button
    submitButton=[[UIButton alloc]init];
    submitButton.frame=CGRectMake(115, 450, 100, 45);
    submitButton.backgroundColor=[UIColor clearColor];
    [submitButton setFont:[UIFont boldSystemFontOfSize:30]];
    [submitButton addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];

    [submitButton setBackgroundImage:[UIImage imageNamed:
                                   @"submit.png"]forState:UIControlStateNormal];
    [self.view addSubview:submitButton];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) submit:(UIButton *)sender
{
    if ([firstNameTextField.text length] == 0) {
        
        [self showerroralert];
        
    }
    
    else if ([lastNameTextField.text length] == 0){
        
        
        
        [self showerroralert];
        
    }
    
    
    
    else if ([emailTextField.text length] == 0){
        
        [self showerroralert];
        
    }
    
    
    
    else if ([phoneNumberTextField.text length] <10){
        
        [self showerroralert];
        
        
        
    }
    
    
    
    else if ([passwordTextField.text length] <8){
        
        [self passwordLength];
        
        
        
    }
    else if ([reEnterPasswordTextField.text length] <8){
        
        [self passwordLength];
        
        
        
    }
    else if ([self checkName: firstNameTextField.text] == YES){
        
        [self alert1];
    }
    
    else if ([self checkName: lastNameTextField.text] == YES){
        
        [self alert2];
    }
    else if ([self checkEmail: emailTextField.text] == YES){
        
        [self alert3];
    }
    else if ([self checkNumber: phoneNumberTextField.text] == YES){
        
        [self alert4];
    }
    else if([passwordTextField.text isEqualToString:reEnterPasswordTextField.text]==NO)
    {
        [self alert5];
    }
    else
    {
        [self alert6];
    }
}

-(void) showerroralert {
    
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"All Field Mandetory" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"continue", nil];
    
    [alert show];
    
}

-(BOOL)checkName:(NSString*)text{
    NSCharacterSet *c=[[NSCharacterSet characterSetWithCharactersInString:@"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM"]invertedSet];
    if ([text rangeOfCharacterFromSet:c].location==NSNotFound) {
        NSLog(@"No Speacial Character");
    } else {
        NSLog(@"Speacial Character Found");
        return YES;
    }
    return NO;
}

-(BOOL)checkEmail:(NSString*)text{
    NSCharacterSet *c=[[NSCharacterSet characterSetWithCharactersInString:@"qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123654789@_."]invertedSet];
    if ([text rangeOfCharacterFromSet:c].location==NSNotFound) {
        NSLog(@"No Speacial Character");
    } else {
        NSLog(@"Speacial Character Found");
        return YES;
    }
    return NO;
}

-(BOOL)checkNumber:(NSString*)text{
    NSCharacterSet *c=[[NSCharacterSet characterSetWithCharactersInString:@"0123654789"]invertedSet];
    if ([text rangeOfCharacterFromSet:c].location==NSNotFound) {
        NSLog(@"No Speacial Character");
    } else {
        NSLog(@"Speacial Character Found");
        return YES;
    }
    return NO;
}

-(void)alert1{
    
    UIAlertView *alert1 = [[UIAlertView alloc]initWithTitle:@"FIRST NAME" message:@"Only characters are allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert1 show];
    
}

-(void)alert2{
    
    UIAlertView *alert2 = [[UIAlertView alloc]initWithTitle:@"LAST NAME" message:@"Only characters are allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert2 show];
}

-(void)alert3{
    
    UIAlertView *alert3 = [[UIAlertView alloc]initWithTitle:@"Email" message:@"Invalid email ID" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert3 show];
}

-(void)alert4{
    
    UIAlertView *alert4 = [[UIAlertView alloc]initWithTitle:@"Phone Number" message:@"Only digits are allowed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert4 show];
}

-(void)alert5{
    
    UIAlertView *alert5 = [[UIAlertView alloc]initWithTitle:@"Password" message:@"Password Doesn't Match" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert5 show];
}

-(void)alert6{
    
    UIAlertView *alert6 = [[UIAlertView alloc]initWithTitle:@"Done!" message:@"You Have been registered" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] ;
    [alert6 show];
}

-(void) phoneNumberLength {
    
    
    
    UIAlertView *alert7 = [[UIAlertView alloc]initWithTitle:@"" message:@"Short Password Length" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"Re-Enter", nil];
    
    [alert7 show];
    
}


-(void) passwordLength {
    
    
    
    UIAlertView *alert8 = [[UIAlertView alloc]initWithTitle:@"" message:@"Short Password Length" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"Re-Enter", nil];
    
    [alert8 show];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
